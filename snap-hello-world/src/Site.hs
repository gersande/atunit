module Site where

import Control.Applicative
import Database.PostgreSQL.Simple.FromRow
import qualified Data.Text as T

data Project = Project
               { title :: T.Text
               , description :: T.Text
               }

instance FromRow Project where
  fromRow = Project <$> field <*> field

instance Show Project where
  show (Project title description) =
    "Project { title: " ++ T.unpack title
    ++ ", description: " ++ T.unpack description
    ++ " }\n"
